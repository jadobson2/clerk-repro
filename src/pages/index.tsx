import { Inter } from 'next/font/google'
import { SignOutButton } from '@clerk/nextjs'
import { useEffect, useState } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const [value, setValue] = useState<{ name: string } | null>(null)

  useEffect(() => {
    fetch('/api/hello').then((res) => res.json().then((value) => setValue(value)))
  }, [])

  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
    >
      Signed In<br />
      {value?.name ?? ''}
      <SignOutButton></SignOutButton>
    </main>
  )
}
